package com.service.projectname.controller;



import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class TestTest05 {

        Test05Delegate test05Delegate = new Test05Delegate();


    @Test
    public void testhelloworld(){

        String expactReturnValue = "hello"; // You should put the expect String type value here.

        String returnValue = test05Delegate.helloworld("hello");

        assertEquals(expactReturnValue, returnValue);
    }

}